const jwt = require("jsonwebtoken");

// [Section] JSON Web Tokens
/*
	- JSON Web Token or JWT is a way of securely passing information from the server to the frontend or to other parts of server
	- Information is kept secure through the use of the secret code
	- Only the system that knows the secret code that can decode the encrypted information
    - Imagine JWT as a gift wrapping service that secures the gift with a lock
	- Only the person who knows the secret code can open the lock
	- And if the wrapper has been tampered with, JWT also recognizes this and disregards the gift
	- This ensures that the data is secure from the sender to the receiver
*/

// Token Creation
const secret = "CourseBookingAPI"

module.exports.createAccessToken = (user) => {
    const data = {
        id : user._id,
        email: user.email,
        isAdmin: user.isAdmin
    }

    return jwt.sign(data, secret, {})
}

// Token Verification
/* 
    Analogy:
        Receive the gift and open it to verify if the sender is legitimate and gift was not tampered.
*/
module.exports.verify = (req, res, next) => {
    // Token is retrieved from the request header
    let token = req.headers.authorization;
    console.log(token);

    // Token received and is not undefined.
    if (typeof token != "undefined") {
        console.log(token);

        // "slice" method - removes the bearer prefix
        // Bearer 63c7a590fe4a403a8b52be0e
        token = token.slice(7, token.length);

        // validate the token using the verify method by decrypting the token using the secret code.
        return jwt.verify(token, secret, (error, data) => {
            // if JWT is not valid
            if (error) {
                return res.send({auth: "Failed"});
            // If JWT is valid.
            } else {
                // To proceed to the next middleware function / callback in the route.
                next();
            }
        }) 
    // Token does not exist.
    } else {
        return res.send({auth: "Failed"});
    }
};

// Token Decryption
/* 
    Analogy
        Open the gift and get the content.      
*/

module.exports.decode = (token) => {

    // token received and is not undefined.
    if (typeof token !== "undefined") {
        // removes the bearer prefix
        token = token.slice(7, token.length);

        // verify method
        return jwt.verify(token, secret, (error, data) => {
            if (error) {
                return null;
            } else {
                // decode method - use to obtain info from JWT.
                // complete : true - option allows to return additional information from the JWT token.
                // .payload contains info provided in the "createAccessToken" (id, email, isAdmin)
                return jwt.decode(token, {complete: true}).payload;
            }
        })
    } else {
        return null;
    }
}