const Course = require("../models/Courses.js");

// Create new course
/* 
    Steps:
    1. Create a new Course object using mongoose model.
    2. Save the new Course to the database.
*/

module.exports.addCourse = (data) => {
    // User is an admin
	if (data.isAdmin) {

		// Creates a variable "newCourse" and instantiates a new "Course" object using the mongoose model
		// Uses the information from the request body to provide all the necessary information
		let newCourse = new Course({
			name : data.course.name,
			description : data.course.description,
			price : data.course.price
		});

		// Saves the created object to our database
		return newCourse.save().then((course, error) => {

			// Course creation successful
			if (error) {

				return false;

			// Course creation failed
			} else {

				return true;

			};

		});

	// User is not an admin
	} else {
		return false;
	};

};


// Retrieve all Courses
module.exports.getAllCourses = () => {
    return Course.find({}).then(result => {
        return result;
    });
};

// Retrieve all active courses
module.exports.getActiveCourses = () => {
    return Course.find({isActive : true}).then(result => {
        return result;
    });
};

// Retrieve specific course
module.exports.getCourse = (reqParams) => {
    console.log(reqParams);
    return Course.findById(reqParams.courseId).then(result => {
        return result;
    });
};

// Update a course
/* 
    Steps:
    1. Create a variable "updateCourse" which will contain the info retrieved from the req. body
    2. Find and update the course ID retrieve from the req params and the variable "updatedCourse".
*/
// Info for updating will be coming from URL parameters and request body.
module.exports.updateCourse = (reqParams, reqBody) => {

    // specifiy the fields of the document to be updated.
    let updatedCourse = {
        name : reqBody.name,
        description : reqBody.description,
        price : reqBody.price
    };

    // findyByIdUpdate(document.courseID, updatesToBeApplied)
    return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
        // Course not updated.
        if (error) {
            return false;
        // Course updated successfully.
        } else {
            return true;
        }
    });
};

// Archive a Course
/* 
    Steps:
    1. Create a route for archiving a course. The route must use JWT authentication and obtain the course ID from the URL.
    2. Create a controller method for archiving a course obtaining the course ID from the request params and the course information from the request body.
*/
module.exports.archiveCourse = (data, reqParams, reqBody) => {
    if (data) {
        let archive = {
            isActive : reqBody.isActive
        }

        return Course.findByIdAndUpdate(reqParams.courseId, archive).then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        }); 
    } else {
        return false;
    }
};