const Users = require('../models/Users.js');
const Course = require('../models/Courses.js')
const bcrypt = require('bcrypt');
const auth = require('../auth.js');

// Check if the email already exists
/*
	Steps: 
	1. Use mongoose "find" method to find duplicate emails
	2. Use the "then" method to send a response back to the frontend appliction based on the result of the "find" method
*/
module.exports.checkEmailExists = (reqBody) => {
    return Users.find({email: reqBody.email}).then((result) => {
        if (result.length > 0) {
            return true;
        } else {
            return false;
        }
    })
}

// User registration
/*
	Steps:
	1. Create a new User object using the mongoose model and the information from the request body
	2. Make sure that the password is encrypted
	3. Save the new User to the database
*/
module.exports.registerUser = (reqBody) => {
    let newUser = new Users({
        firstname : reqBody.firstname,
        lastname : reqBody.lastname,
        email : reqBody.email,
        mobileNumber : reqBody.mobileNumber,
        isAdmin : reqBody.isAdmin,
        // Ten is the value provided as the number of "salt"
        password : bcrypt.hashSync(reqBody.password, 10)
    })

    // Saves the created object to our database.
    return newUser.save().then((user, error) => {
        // Registration Failed.
        if (error) {
            return false;
        } else {
            // If registration is successful.
            return true;
        }
    })
}

// User authentication
/*
Steps:
	1. Check the database if the user email exists.
	2. Compare the password provided in the login form with the password stored in the database.
	3. Generate/return a JSON web token if the user is successfully logged in and return false if not.
*/
module.exports.loginUser = (reqBody) => {
    return Users.findOne({email: reqBody.email}).then(result => {
        if (result == null) {
            return false;
        } else {
            const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password);

            if (isPasswordCorrect) {
                return {access: auth.createAccessToken(result)};
            } else {
                return false;
            }
        }
    })
}

// Get Profile
/* 
    Steps:
    1. Find the document in the database using the User's ID.
    2. Reassign the password of the returned document to an empty string.
    3. Return the result back to the frontend.
*/
module.exports.getProfile = (data) => {
    return Users.findById(data.userId).then(result => {
        console.log(result);
        result.password = "";
        return result;
    })
}

// Enroll 
/* 
    Steps:
    1. Find the document in the database using the using user's id.
    2. Add the course id to the user's enrollment array.
    3. Update the document.
*/
// Asynce await - will be used in enrolling the user.
// this will need to update 2 separate documents.
module.exports.enroll = async (data, reqBody) => {

    // creates isUserUpdated variable that will return true upon succesful update.
    // await keyword - will allow the enroll method to complete updating the user before returning a response back to the frontend.
    let isUserUpdated = await Users.findById(data.id).then(user => {

        // adds the courseId to the user's enrollment array.
        user.enrollments.push({courseID: reqBody.courseID});

        // saves the updated user information in the database.
         return  user.save().then((user, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })

    // adds the user id in the enrolless array of the course.
    // await - to allow the enroll method to complete updating the course before returning a response to the frontend.
    let isCourseUpdated = await Course.findById(reqBody.courseID).then(course => {

        // add the user id in the course's enrollees array.
        course.enrollees.push({userId: data.id});
            
        // saves the updated course info in the database.
        return course.save().then((course, error) => {
            if (error) {
                return false;
            } else {
                return true;
            }
        })
    })

    // Condition will check if the user and the course documents have been updated.
    if (isUserUpdated && isCourseUpdated) {
        return true;
    } else {
        return false;
    };
}