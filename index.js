const express = require('express');
const mongoose = require('mongoose');
const cors = require('cors');
const userRoutes = require('./routes/user.js');
const courseRoutes = require('./routes/course.js');

const app = express();


// Database Connection
mongoose.connect("mongodb+srv://admin:admin1234@cluster0.nk0iigs.mongodb.net/s37-s41?retryWrites=true&w=majority", {
    useNewUrlParser : true, 
    useUnifiedTopology : true
});

// Set notification for connection success or failure
let db = mongoose.connection;
db.on("error", console.error.bind(console, "connection error"));
db.once("open", () => console.log("We're connected to the cloud database."));

// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use('/users', userRoutes);
app.use('/courses', courseRoutes);

// Server Listening
// app.listen(port, () => console.log(`Now listening to port ${port}`));
app.listen(process.env.PORT || 4000, () => {
    console.log(`API is now online on port ${process.env.PORT || 4000}`);
})