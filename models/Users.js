const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
    firstname : {
        type : String,
        required : [true, `First name is required!`]
    },
    lastname : {
        type : String,
        required : [true, `Last name is required!`]
    },
    email : {
        type : String,
        required : [true, `Email name is required!`]
    },
    password : {
        type : String,
        required : [true, `Password name is required!`]
    },
    isAdmin : {
        type : Boolean,
        required: false
    },
    mobileNumber : {
        type : String,
        required: [true, "Mobile number is required!"]
    },
    enrollments : [
        {
            courseID : {
                type: String,
                required : [true, `Course ID is required`]
            },
            enrolledOn : {
                type : Date,
                default : new Date()
            },
            status : {
                type : String,
                default : "Enrolled"
            }
        }
    ]
})

module.exports = mongoose.model("User", userSchema);