const express = require('express');
const router = express.Router();
const userControllers = require('../controllers/userControllers.js')
const auth = require('../auth.js');

// Check email if it is existing to our database.
router.post('/checkEmail', (req, res) => {
    userControllers.checkEmailExists(req.body).then(resultFromController => 
        res.send(resultFromController)
    );
});

// User Registration Route
router.post('/register', (req, res) => {
    userControllers.registerUser(req.body).then(resultFromController => 
        res.send(resultFromController)
    );
});

// User Login
router.post('/login', (req, res) => {
    userControllers.loginUser(req.body).then(resultFromController => 
        res.send(resultFromController)
    );
});

// Retrieving user details
// auth.verify - middleware to ensure that the user is logged in before they can retrieve their details.
router.post('/details', auth.verify, (req, res) => {
    // uses the decode method define in the auth.js to retreive user info from request header.
    const userData = auth.decode(req.headers.authorization);

    userControllers.getProfile({userId: req.body.id})
        .then(resultFromController => res.send(resultFromController));
});


// Enroll a user
router.post('/enroll', auth.verify, (req, res) => {
    // the values needed to enroll, one for the user who will enroll, one for the course that they would like to enroll in.
    // let data = {
    //     userId: req.body.userId,
    //     courseID: req.body.courseID
    // }
    // userControllers
    //     .enroll(data)
    //     .then(resultFromController => res.send(resultFromController));

    let data = auth.decode(req.headers.authorization);

    userControllers.enroll(data, req.body).then(resultFromController => res.send(resultFromController));

})

// this is to connect the route to other modules.
module.exports = router;